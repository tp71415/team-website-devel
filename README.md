Contribution please! :)

## Installation guide

Please note that grunt is required to develop and build this project. Make sure you have nodejs installed on your system, then run

```javascript
npm install -g grunt-cli
```

After that, proceed

1. Clone this repo and navigate to the directory
2. Run:
```javascript
npm install
```
This will download local nodejs modules required to run grunt tasks.
3. Then run
```javascript
bower install
```
To install all code dependencies, like bootstrap etc.
4. Profit - described below

## Usage

Gruntfile contains some useful tasks:

1. `grunt serve` for development - will create a local dev server and auto-udpate tab in browser on file change
2. `grunt build` for building from source to `dist` folder
3. `grunt wiredep` to make bower inject dependencies into index.html

.. and many more! Ask @PatrikGallik