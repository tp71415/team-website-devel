
(function($) {

  var nav = $(".masthead");

  $(window).scroll(function() {

    if (nav.offset().top > 50) {
      nav.addClass("with-background");
    } else {
      nav.removeClass("with-background");
    }

  });

  $('a.page-scroll').on('click', function(e) {
    e.preventDefault();

    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top - 75
    }, 500, 'swing');
  });

})(jQuery);

